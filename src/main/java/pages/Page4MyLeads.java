package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPom;

public class Page4MyLeads extends ProjectMethodsPom{
	public Page4MyLeads() {
		PageFactory.initElements(driver, this);
		}
		@FindBy(how=How.LINK_TEXT, using ="Create Lead") WebElement eleCreateLead;
		public Page5CreateLead eleCreateLead() {
			click(eleCreateLead);
			return new Page5CreateLead();
		}
		
		
}
