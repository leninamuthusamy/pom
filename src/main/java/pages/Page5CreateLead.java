package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPom;

public class Page5CreateLead extends ProjectMethodsPom{
	public Page5CreateLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.CLASS_NAME, using ="companyName") WebElement eleCompanyName;
	
	@FindBy(how=How.ID, using ="createLeadForm_firstName") WebElement eleFirstName;
	
	@FindBy(how=How.ID, using ="createLeadForm_lastName") WebElement eleLastName;
	
	@FindBy(how=How.CLASS_NAME, using ="smallSubmit") WebElement eleCreateLeadbutton;

	
	public Page5CreateLead enterCompanyName(String uCompanyName) {
		type(eleCompanyName, uCompanyName);
		return this;
	}
	public Page5CreateLead enterFirstName(String uFirstName) {
		type(eleFirstName, uFirstName);
		return this;
	}
	public Page5CreateLead enterLastName(String uLastName) {
		type(eleLastName, uLastName);
		return this;
	}
	public Page6ViewLead clickCreateLeadbutton() {
		click(eleCreateLeadbutton);
		return new Page6ViewLead();
	}
}
