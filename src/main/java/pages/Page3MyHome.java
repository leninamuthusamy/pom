package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPom;

public class Page3MyHome extends ProjectMethodsPom{
	public Page3MyHome() {
		PageFactory.initElements(driver, this);
	}
		@FindBy(how=How.LINK_TEXT, using="Leads") WebElement eleLeads;
		
		public Page4MyLeads eleLeads() {
			click(eleLeads);
			return new Page4MyLeads();
		}

}
