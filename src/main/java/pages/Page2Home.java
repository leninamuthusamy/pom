package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPom;

public class Page2Home extends ProjectMethodsPom{
	public Page2Home() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT, using ="CRM/SFA") WebElement eleCRMSFA;
	
	public Page3MyHome clickCRMSFA() {
		click(eleCRMSFA);
		return new Page3MyHome();
	}
	}
	


