import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public final class LearnWindowHandles {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignup.jsf");
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		System.out.println("IRCTC - Agent Login web page is loaded successfully!");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//a[text()='Contact Us']").click();
		Set<String> contactus = driver.getWindowHandles();
		List<String> lst = new ArrayList();
		lst.addAll(contactus);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File("./Screenshot/contactus.jpeg");
		FileUtils.copyFile(src, obj);
		driver.switchTo().window(lst.get(0));
		driver.close();
		

		
	}

}
